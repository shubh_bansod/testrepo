/*
SQLyog Ultimate v12.09 (32 bit)
MySQL - 5.5.48-log : Database - entertain
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`entertain` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `entertain`;

/*Table structure for table `eworld` */

DROP TABLE IF EXISTS `eworld`;

CREATE TABLE `eworld` (
  `name` char(50) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `mobileno` varchar(50) NOT NULL,
  `password` varchar(50) DEFAULT NULL,
  `Balance` double DEFAULT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `eworld` */

insert  into `eworld`(`name`,`email`,`mobileno`,`password`,`Balance`) values ('shubh','shubhambansod007@gmail.com','7509494579','shub4252',7000),('sonali choudhary','sonali@gmail.com','9879456412','sona5988',5500);

/*Table structure for table `history` */

DROP TABLE IF EXISTS `history`;

CREATE TABLE `history` (
  `Email` varchar(50) NOT NULL,
  `service` varchar(50) DEFAULT NULL,
  `Debited` double NOT NULL,
  `Balance` double NOT NULL,
  `Date` date NOT NULL,
  `Time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `history` */

insert  into `history`(`Email`,`service`,`Debited`,`Balance`,`Date`,`Time`) values ('shubhambansod007@gmail.com','airtel_recharge',1000,3000,'2017-07-23','23:02:31'),('shubhambansod007@gmail.com','idea_recharge',1000,7000,'2017-07-23','23:04:40'),('sonali@gmail.com','idea',1000,6000,'2017-07-23','23:11:49'),('shubhambansod007@gmail.com','airtel',500,6000,'2017-07-24','00:16:07'),('shubhambansod007@gmail.com','reliance',500,5500,'2017-07-24','00:20:19'),('shubhambansod007@gmail.com','jio',100,5400,'2017-07-24','00:23:34'),('shubhambansod007@gmail.com','vodafone',100,5300,'2017-07-24','00:26:27'),('shubhambansod007@gmail.com','movie_at_pvr',600,3700,'2017-07-24','00:48:37'),('shubhambansod007@gmail.com','airtel',100,3500,'2017-07-24','00:52:53'),('shubhambansod007@gmail.com','munaa_at_pvr',120,3380,'2017-07-24','00:53:56'),('shubhambansod007@gmail.com','movie_avengers_at_INOX',500,2380,'2017-07-24','01:01:19'),('shubhambansod007@gmail.com','jio_recharge',400,7000,'2017-07-24','01:04:31'),('sonali@gmail.com','Movie_at_PVR_with_sona',500,5500,'2017-07-24','01:06:45');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
