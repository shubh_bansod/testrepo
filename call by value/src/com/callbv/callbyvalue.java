package com.callbv;

public class callbyvalue {

	int i;
	int j;
	public void putdata(int a, int b)
	{
		i=a;
		j=b;
		++i;
		++j;
		++a;
		++b;
		System.out.println("a="+a+"b="+b);
	}
}
class main{
	public static void main(String args[]){
		int x=10;
		int y=20;
		callbyvalue cbv=new callbyvalue();
		cbv.putdata(x, y);
		System.out.println("x="+x);
		System.out.println("y="+y);
	}
}